#include <iostream>
#include <omp.h>
#include <fstream>
#include <vector>
#include <fstream>

using namespace std;

bool GCD(unsigned long a, unsigned long b)
{
	if (b == 0)
		return a == 1;
	return GCD(b, a % b);
}

int main(int argc, char* argv[])
{
	ifstream in;
	ofstream out;
	in.open(argv[2]);
	out.open(argv[3]);
	vector<unsigned long> A;
	vector<unsigned long> B;
	vector<int> result;
	int size;
	in >> size;
	if (size < 1000)
	{
		cout << "Wrong size: must be >= 1000.";
		return 1;
	}
	for (int i = 0; i < size; i++)
	{
		unsigned long val;
		in >> val;
		A.push_back(val);
	}
	for (int i = 0; i < size; i++)
	{
		unsigned long val;
		in >> val;
		B.push_back(val);
	}
	
	int threadsNumber = atoi(argv[1]);

#pragma omp parallel num_threads(threadsNumber)
	{
#pragma omp for
		for (int i = 0; i < size; i++)
		{
			const unsigned long a = A[i];
			const unsigned long b = B[i];
#pragma omp critical (out)
			if (GCD(a, b) == 1)
				out << i << '\n';
		}
	}
}
